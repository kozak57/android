package com.example.alexey.lesson4.service;

import android.util.Log;

public class MyThread implements Runnable {

    private String name;
    private final long timeSleep;
    private static final String TAG = "MyThread";

    public MyThread(long timeSleep, String name) {
        this.timeSleep = timeSleep;
        this.name = name;
    }

    @Override
    public void run() {

        Log.d(TAG, "Thread  " + name + " is working");


        try {

            Thread.sleep(this.timeSleep);

        } catch (InterruptedException e) {

            e.printStackTrace();

        }
        Log.d(TAG, "Thread  " + name + " complete work");

    }

}
