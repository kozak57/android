package com.example.alexey.lesson4;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.example.alexey.lesson4.restServices.CurrencyAdapter;
import com.example.alexey.lesson4.restServices.CurrencyService;
import com.example.alexey.lesson4.restServices.DataLoadedEvent;
import com.example.alexey.lesson4.restServices.EDBus;
import com.squareup.otto.Subscribe;


public class CurrencyActivity extends Activity {
    private CurrencyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency);


        adapter = new CurrencyAdapter(getBaseContext());

        ListView currencyList = (ListView) findViewById(R.id.currency_list);
        currencyList.setAdapter(adapter);

        Intent intent = new Intent(this, CurrencyService.class);
        startService(intent);

    }

    @Override
    protected void onPause() {
        super.onPause();
        EDBus.getBusInstance().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EDBus.getBusInstance().register(this);
    }

    @Subscribe
    public void updateData(DataLoadedEvent dataLoadedEvent) {
        adapter.setDataListCurrencyInfo(dataLoadedEvent.getCurrencyInfos());
    }

}
