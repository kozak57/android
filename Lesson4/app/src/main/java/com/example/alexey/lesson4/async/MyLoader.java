package com.example.alexey.lesson4.async;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.database.Cursor;

import com.example.alexey.lesson4.db.MySQLiteHelper;


public class MyLoader extends AsyncTaskLoader<Cursor> {
    private MySQLiteHelper db = new MySQLiteHelper(getContext());

    public MyLoader(Context context) {
        super(context);
    }

    @Override
    public Cursor loadInBackground() {

        return db.getCursorWithFilter(null);
    }
}
