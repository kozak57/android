package com.example.alexey.lesson4.fragments;

import com.example.alexey.lesson4.dto.Details;

public interface FragmentCommunication {
    void showDetails(Details details);
}
