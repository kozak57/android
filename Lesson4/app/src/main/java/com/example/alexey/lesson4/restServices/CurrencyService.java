package com.example.alexey.lesson4.restServices;

import android.app.IntentService;
import android.content.Intent;

import com.example.alexey.lesson4.restServices.retrofit.PBServiceAdapter;

import java.io.IOException;
import java.util.List;

import retrofit.Response;

public class CurrencyService extends IntentService {


    public CurrencyService() {
        super("");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        PBServiceAdapter pbServiceAdapter = new PBServiceAdapter();
        Response<List<CurrencyInfo>> listResponse = null;


        try {
            listResponse = pbServiceAdapter.getService().getContent().execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        assert listResponse != null;
        EDBus.getBusInstance().post(new DataLoadedEvent(listResponse.body()));


    }
}
