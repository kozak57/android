package com.example.alexey.lesson4.db;

import android.provider.BaseColumns;


public class MyDbContract {
    private MyDbContract() {}

    public static abstract class Item implements BaseColumns {
        public static final String TABLE_NAME = "item";

        public static final String NAME = "item_name";
        public static final String DESC = "item_desc";
        public static final int ITEM_ID_PATH_POSITION = 0;
    }
}
