package com.example.alexey.lesson4.fragments;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alexey.lesson4.R;
import com.example.alexey.lesson4.dto.Details;


public class ItemDetailedFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.detailed_fragment, container, false);
    }

    public void setDetails(Details details) {
        TextView nameTextView = (TextView) this.getView().findViewById(R.id.nameText);
        TextView descTextView = (TextView) this.getView().findViewById(R.id.descText);

        nameTextView.setText(details.getName());
        descTextView.setText(details.getDescription());
    }
}
