package com.example.alexey.lesson4;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ItemCursorAdapter extends CursorAdapter {

    public ItemCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_layout, parent, false);

        TextView nameTextView = (TextView) view.findViewById(R.id.item_name);
        TextView descTextView = (TextView) view.findViewById(R.id.item_description);
        view.setTag(new ViewHolder(nameTextView, descTextView));

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView itemName;
        TextView itemDescription;
        ViewHolder viewHolder = (ViewHolder) view.getTag();

        itemName = viewHolder.getNameTextView();
        itemDescription = viewHolder.getDescTextView();

        itemName.setText(cursor.getString(cursor.getColumnIndex("item_name")));
        itemDescription.setText(cursor.getString(cursor.getColumnIndex("item_desc")));
    }

    static class ViewHolder {
        TextView nameTextView;
        TextView descTextView;

        public ViewHolder(TextView nameTextView, TextView descTextView) {
            this.nameTextView = nameTextView;
            this.descTextView = descTextView;
        }

        public TextView getNameTextView() {
            return nameTextView;
        }

        public TextView getDescTextView() {
            return descTextView;
        }
    }
}