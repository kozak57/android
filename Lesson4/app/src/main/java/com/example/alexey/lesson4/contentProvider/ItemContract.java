package com.example.alexey.lesson4.contentProvider;


import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class ItemContract {
    public static final String AUTHORITY = "com.example.alexey.lesson4.contentProvider.ItemContract";
    private static final String SCHEME = "content://";

    public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + Items.PATH_ITEM_ID);

    public static final class Items implements BaseColumns {
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/com.example.alexey.lesson4.contentProvider";
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/com.example.alexey.lesson4.contentProvider";
        public static final String ITEM_NAME = "item_name";
        public static final String ITEM_DESC = "item_desc";
        public static final String DEFAULT_SORT_ORDER = ITEM_NAME + " ASC";
        private static final String PATH_ITEM_ID = "/items/";
        public static final String[] DEFAULT_PROJECTION = new String[]{
                Items._ID,
                Items.ITEM_NAME,
                Items.ITEM_DESC
        };

    }
}
