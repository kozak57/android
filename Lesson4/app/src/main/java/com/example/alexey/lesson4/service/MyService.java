package com.example.alexey.lesson4.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class MyService extends Service {

    private ExecutorService executor;
    private MyThread worker1;
    private MyThread worker2;
    private MyThread worker3;
    private static final String TAG = "MyService";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBindService");
        return null;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreateService");
        super.onCreate();
        executor = Executors.newFixedThreadPool(6);
        worker1 = new MyThread(5000, "Worker 1");
        worker2 = new MyThread(3000, "Worker 2");
        worker3 = new MyThread(6000, "Worker 3");

    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {

        executor.execute(worker1);
        executor.execute(worker2);
        executor.execute(worker3);

        return Service.START_NOT_STICKY;

    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        executor.shutdown();
        Log.wtf(TAG, "SERVICE STOPPED!!!");
        super.onDestroy();
    }


}
