package com.example.alexey.lesson4;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.ListView;

import com.example.alexey.lesson4.contentProvider.ItemContract;
import com.example.alexey.lesson4.db.MySQLiteHelper;
import com.example.alexey.lesson4.dto.Details;
import com.example.alexey.lesson4.fragments.FragmentCommunication;
import com.example.alexey.lesson4.fragments.ItemDetailedFragment;
import com.example.alexey.lesson4.fragments.ItemListFragment;
import com.example.alexey.lesson4.service.MyService;

import java.util.Date;

public class MainActivity extends Activity implements FragmentCommunication, LoaderManager.LoaderCallbacks<Cursor> {

    private MySQLiteHelper db;
    private ItemCursorAdapter cursorAdapter;
    private Intent intent;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startActivity(new Intent(this,RestActivity.class));

        Log.d(TAG, "MainActivity onCreate starts");
        getLoaderManager().initLoader(0, null, this);

        db = new MySQLiteHelper(this);

        //Cursor cursor = db.getCursorAllItems();
        //cursorAdapter = new ItemCursorAdapter(getBaseContext(), cursor, true);
        cursorAdapter = new ItemCursorAdapter(getBaseContext(), null, true);

        ListView itemList = (ListView) findViewById(R.id.list);
        itemList.setAdapter(cursorAdapter);
        Log.d(TAG, "cursor is setting to array adapter");
        Log.i(TAG, "Current time"+ new Date().getTime());


        EditText filterInput = (EditText) findViewById(R.id.editText);

//        filterInput.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                Log.d(TAG, "Filter text changed");
//                cursorAdapter.getFilter().filter(s.toString());
//            }
//        });
//
//        cursorAdapter.setFilterQueryProvider(new FilterQueryProvider() {
//            @Override
//            public Cursor runQuery(CharSequence constraint) {
//                return db.getCursorWithFilter(constraint.toString());
//            }
//        });


        intent = new Intent(this, MyService.class);
        startService(intent);


    }

    @Override
    public void showDetails(Details details) {
        ItemDetailedFragment itemDetailedFragment = (ItemDetailedFragment) getFragmentManager().findFragmentById(R.id.detailed_fragment);

        ItemListFragment itemListFragment = (ItemListFragment) getFragmentManager().findFragmentById(R.id.list_fragment);
        if (itemDetailedFragment != null) {

            if (itemDetailedFragment.isInLayout()) {
                itemDetailedFragment.setDetails(details);
            }
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.hide(itemListFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                itemDetailedFragment.setDetails(details);
            }
        }
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "Stop activity");
        super.onStop();
        stopService(intent);
        db.closeConnection();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.d(TAG, "Creating cursor");
        return new CursorLoader(
                this,
                ItemContract.CONTENT_URI,
                ItemContract.Items.DEFAULT_PROJECTION,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        cursorAdapter.changeCursor(data);
        Log.d(TAG, "Cursor changed");
        loader.reset();
    }

    @Override
    public void onLoaderReset(Loader loader) {
        cursorAdapter.swapCursor(null);
    }
}
