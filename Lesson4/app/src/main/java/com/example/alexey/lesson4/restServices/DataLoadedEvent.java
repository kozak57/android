package com.example.alexey.lesson4.restServices;


import android.util.Log;

import java.util.List;

public class DataLoadedEvent {
    private static final String TAG = "DataLoadedEvent";
    private List<CurrencyInfo> currencyInfos;

    public DataLoadedEvent(List<CurrencyInfo> currencyInfos) {

        Log.d(TAG, "data loaded");
        this.currencyInfos = currencyInfos;
    }

    public List<CurrencyInfo> getCurrencyInfos() {
        return currencyInfos;
    }
}
