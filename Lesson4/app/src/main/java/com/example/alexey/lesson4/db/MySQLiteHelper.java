package com.example.alexey.lesson4.db;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {

    private static final String TAG = "MySQLiteHelper";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Item.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + MyDbContract.Item.TABLE_NAME + " (" +
                    MyDbContract.Item._ID + " INTEGER PRIMARY KEY," +
                    MyDbContract.Item.NAME + TEXT_TYPE + COMMA_SEP +
                    MyDbContract.Item.DESC + TEXT_TYPE +
                    " )";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "Creating database");
        db.execSQL(SQL_CREATE_ENTRIES);
        fillInitialData(db);
    }

    private void fillInitialData(SQLiteDatabase db) {
        ContentValues values = new ContentValues();

        Log.d(TAG, "Put initial data into db");

        values.put(MyDbContract.Item.NAME, "Item 1");
        values.put(MyDbContract.Item.DESC, "Item 1 desc");
        db.insert(MyDbContract.Item.TABLE_NAME, null, values);
        values.clear();

        values.put(MyDbContract.Item.NAME, "Item 2");
        values.put(MyDbContract.Item.DESC, "Item 2 desc");
        db.insert(MyDbContract.Item.TABLE_NAME, null, values);
        values.clear();

        values.put(MyDbContract.Item.NAME, "Item 3");
        values.put(MyDbContract.Item.DESC, "Item 3 desc");
        db.insert(MyDbContract.Item.TABLE_NAME, null, values);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + MyDbContract.Item.TABLE_NAME);
        onCreate(db);
    }

    public Cursor getCursorAllItems() {
        Log.d(TAG, "getting all items from db");
        return this.getReadableDatabase().rawQuery("SELECT * FROM " + MyDbContract.Item.TABLE_NAME, null);
    }

    public Cursor getCursorWithFilter(String filter) {
        Cursor cursor;
        String query;
        if (filter.length() == 0) {
            cursor = getCursorAllItems();
        } else {
            Log.d(TAG, "getting item from db by name");
            query = "SELECT * FROM " + MyDbContract.Item.TABLE_NAME + " WHERE " + MyDbContract.Item.NAME + " LIKE '" + filter + "%'";
            cursor = this.getReadableDatabase().rawQuery(query, null);
        }
        return cursor;
    }

//    public void createItem(Item item) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(MyDbContract.Item.NAME, item.getName());
//        values.put(MyDbContract.Item.DESC, item.getDescription());
//
//        db.insert(MyDbContract.Item.TABLE_NAME, null, values);
//    }
//
//    public Item getItem(int id) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.query(MyDbContract.Item.TABLE_NAME,
//                new String[]{MyDbContract.Item.NAME,
//                        MyDbContract.Item.DESC}, "_ID = ?",
//                new String[]{String.valueOf(id)},
//                null, null, null);
//        if (cursor != null) {
//            cursor.moveToFirst();
//        }
//        Item item = new Item(cursor.getString(0), cursor.getString(1));
//        return item;
//    }
//
//    public List<Item> getAllItems() {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        List<Item> items = new ArrayList<>();
//
//        String query = "SELECT * FROM " + MyDbContract.Item.TABLE_NAME;
//        Cursor cursor = db.rawQuery(query, null);
//
//        if (cursor.moveToFirst()) {
//            do {
//                Item item = new Item();
//                item.setName(cursor.getString(0));
//                item.setDescription(cursor.getString(1));
//                items.add(item);
//            } while (cursor.moveToNext());
//        }
//        return items;
//    }
//
//    public void updateItem(Item item, int id) {
//
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(MyDbContract.Item.NAME, item.getName());
//        values.put(MyDbContract.Item.DESC, item.getDescription());
//
//        db.update(MyDbContract.Item.TABLE_NAME, values, "_ID = ?", new String[]{String.valueOf(id)});
//    }
//
//    public void deleteItem(int id) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete(MyDbContract.Item.TABLE_NAME, "_ID = ?", new String[]{String.valueOf(id)});
//    }

    public void closeConnection() {
        Log.d(TAG, "closing db connection");
        this.close();
    }
}
