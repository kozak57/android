package com.example.alexey.lesson4.contentProvider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.alexey.lesson4.db.MyDbContract;
import com.example.alexey.lesson4.db.MySQLiteHelper;

import java.util.HashMap;

public class MyContentProvider extends ContentProvider {

    private static final int ITEM_LIST = 1;
    private static final int ITEM_ID = 2;
    private static UriMatcher uriMatcher;
    private MySQLiteHelper sqLiteHelper;
    private static HashMap<String, String> itemProjectionMap;

    private static final String TAG = "MyContentProvider";


    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(ItemContract.AUTHORITY, "items", ITEM_LIST);
        uriMatcher.addURI(ItemContract.AUTHORITY, "items/#", ITEM_ID);
        itemProjectionMap = new HashMap<>();
        for (int i = 0; i < ItemContract.Items.DEFAULT_PROJECTION.length; i++) {
            itemProjectionMap.put(ItemContract.Items.DEFAULT_PROJECTION[i], ItemContract.Items.DEFAULT_PROJECTION[i]);
        }
    }


    @Override
    public boolean onCreate() {
        Log.d(TAG, "Creating MyContentProvider");
        sqLiteHelper = new MySQLiteHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String orderBy = null;
        switch (uriMatcher.match(uri)) {
            case ITEM_LIST:
                qb.setTables(MyDbContract.Item.TABLE_NAME);
                qb.setProjectionMap(itemProjectionMap);
                orderBy = ItemContract.Items.DEFAULT_SORT_ORDER;

                Log.d(TAG, "Created ItemList query");

                break;
            case ITEM_ID:
                qb.setTables(MyDbContract.Item.TABLE_NAME);
                qb.setProjectionMap(itemProjectionMap);
                qb.appendWhere(ItemContract.Items._ID + "=" + uri.getPathSegments().get(MyDbContract.Item.ITEM_ID_PATH_POSITION));
                orderBy = ItemContract.Items.DEFAULT_SORT_ORDER;

                Log.d(TAG, "Created ItemId query");

                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);

        }
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        Cursor cursor = qb.query(db, projection, selection, selectionArgs, null, null, orderBy);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        Log.d(TAG, "Created cursor");
        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case ITEM_LIST:
                return ItemContract.Items.CONTENT_TYPE;
            case ITEM_ID:
                return ItemContract.Items.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        if (uriMatcher.match(uri) != ITEM_LIST) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
        SQLiteDatabase db = sqLiteHelper.getWritableDatabase();
        ContentValues values;
        if (initialValues != null) {
            values = new ContentValues(initialValues);
        } else {
            values = new ContentValues();
        }

        long rowId = -1;
        Uri rowUri = Uri.EMPTY;

        switch (uriMatcher.match(uri)) {
            case ITEM_LIST:
                if (!values.containsKey(ItemContract.Items.ITEM_NAME)) {
                    values.put(ItemContract.Items.ITEM_NAME, "");
                }
                if (!values.containsKey(ItemContract.Items.ITEM_DESC)) {
                    values.put(ItemContract.Items.ITEM_DESC, "");
                }

                rowId = db.insert(MyDbContract.Item.TABLE_NAME, ItemContract.Items.ITEM_NAME, values);
                if (rowId > 0) {
                    rowUri = ContentUris.withAppendedId(ItemContract.CONTENT_URI, rowId);
                    getContext().getContentResolver().notifyChange(rowUri, null);
                }
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        return rowUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = sqLiteHelper.getWritableDatabase();
        int count;
        String finalSelection;

        switch (uriMatcher.match(uri)) {
            case ITEM_LIST:
                count = db.delete(MyDbContract.Item.TABLE_NAME, selection, selectionArgs);
                break;
            case ITEM_ID:
                finalSelection = ItemContract.Items._ID + " = " + uri.getPathSegments().get(MyDbContract.Item.ITEM_ID_PATH_POSITION);
                if (selection != null) {
                    finalSelection = finalSelection + " AND " + selection;
                }
                count = db.delete(MyDbContract.Item.TABLE_NAME, finalSelection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = sqLiteHelper.getWritableDatabase();
        int count;
        String finalSelection;
        String id;

        switch (uriMatcher.match(uri)) {
            case ITEM_LIST:
                count = db.update(MyDbContract.Item.TABLE_NAME, values, selection, selectionArgs);
                break;
            case ITEM_ID:
                id = uri.getPathSegments().get(MyDbContract.Item.ITEM_ID_PATH_POSITION);
                finalSelection = ItemContract.Items._ID + " = " + id;
                if (selection != null) {
                    finalSelection = finalSelection + " AND " + selection;
                }
                count = db.update(MyDbContract.Item.TABLE_NAME, values, finalSelection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
}
