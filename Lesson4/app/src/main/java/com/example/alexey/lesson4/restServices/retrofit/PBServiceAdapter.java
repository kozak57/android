package com.example.alexey.lesson4.restServices.retrofit;


import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class PBServiceAdapter {
    private static final String BASE_URL = "https://api.privatbank.ua";

    private PBService service;

    public PBServiceAdapter() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(PBService.class);

    }

    public PBService getService() {
        return service;
    }


}
