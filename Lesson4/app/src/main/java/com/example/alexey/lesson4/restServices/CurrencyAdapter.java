package com.example.alexey.lesson4.restServices;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.alexey.lesson4.R;

import java.util.ArrayList;
import java.util.List;


public class CurrencyAdapter extends BaseAdapter {

    Context context;
    List<CurrencyInfo> currencyInfoList = new ArrayList<>();

    public CurrencyAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return currencyInfoList.size();
    }

    @Override
    public Object getItem(int position) {

        return currencyInfoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.currency_layout, parent, false);

        TextView ccyTextView = (TextView) view.findViewById(R.id.ccy);
        TextView buyTextView = (TextView) view.findViewById(R.id.buy);
        TextView saleTextView = (TextView) view.findViewById(R.id.sale);
        view.setTag(new ViewHolder(ccyTextView, buyTextView, saleTextView));

        ccyTextView.setText(currencyInfoList.get(position).getCcy());
        buyTextView.setText(currencyInfoList.get(position).getBuy());
        saleTextView.setText(currencyInfoList.get(position).getSale());

        return view;
    }

    static class ViewHolder {
        TextView ccyTextView;
        TextView buyTextView;
        TextView saleTextView;


        public ViewHolder(TextView ccyTextView, TextView buyTextView, TextView saleTextView) {
            this.ccyTextView = ccyTextView;
            this.buyTextView = buyTextView;
            this.saleTextView = saleTextView;
        }

        public TextView getCcyTextView() {
            return ccyTextView;
        }

        public TextView getBuyTextView() {
            return buyTextView;
        }

        public TextView getSaleTextView() {
            return saleTextView;
        }
    }

    public void setDataListCurrencyInfo(List<CurrencyInfo> listCurrencyInfo) {
        this.currencyInfoList = listCurrencyInfo;
        notifyDataSetChanged();
    }
}
