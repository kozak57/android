package com.example.alexey.lesson4.restServices.retrofit;


import com.example.alexey.lesson4.restServices.CurrencyInfo;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;

public interface PBService {

    @GET("p24api/pubinfo?json&exchange&coursid=5")
    Call<List<CurrencyInfo>> getContent();
}
