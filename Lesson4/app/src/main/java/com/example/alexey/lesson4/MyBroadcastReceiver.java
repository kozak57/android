package com.example.alexey.lesson4;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;


public class MyBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "BroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG,"Action received");
        Toast toast = Toast.makeText(context, "Application boot complete", Toast.LENGTH_SHORT);
        toast.show();
    }
}
