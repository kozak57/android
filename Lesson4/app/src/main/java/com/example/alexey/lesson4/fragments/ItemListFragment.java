package com.example.alexey.lesson4.fragments;


import android.app.Fragment;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.alexey.lesson4.Item;
import com.example.alexey.lesson4.R;
import com.example.alexey.lesson4.dto.Details;

import java.util.Objects;

public class ItemListFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);

        ListView listView = (ListView) view.findViewById(R.id.list);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (getActivity() instanceof FragmentCommunication) {
                    Cursor cursorItem = (Cursor) parent.getAdapter().getItem(position);
                    String name = cursorItem.getString(1);
                    String description = cursorItem.getString(2);
                    ((FragmentCommunication) getActivity()).showDetails(new Details(name, description));
                }
            }
        });

        return view;
    }
}
