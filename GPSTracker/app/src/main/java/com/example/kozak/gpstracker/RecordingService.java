package com.example.kozak.gpstracker;


import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.example.kozak.gpstracker.edbus.EDBus;
import com.example.kozak.gpstracker.edbus.GpsDataReceivedEvent;
import com.example.kozak.gpstracker.edbus.PauseStatusChangedEvent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.squareup.otto.Subscribe;

public class RecordingService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    public static final String TAG = "RecordTrackActivity";
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;

    @Override
    public void onCreate() {
        super.onCreate();
        EDBus.getInstance().register(this);

        locationRequest = LocationRequest.create();
        locationRequest.setInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        googleApiClient.connect();
        Log.d(TAG, "Google api client created");
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "Location request created");

        requestLocation();
    }

    @Override
    public void onDestroy() {
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        googleApiClient.disconnect();
        EDBus.getInstance().unregister(this);
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
        String coordinates = String.valueOf(location.getLongitude()) + "/" + String.valueOf(location.getLatitude());
        EDBus.getInstance().post(new GpsDataReceivedEvent(location));
        Log.d(TAG, coordinates);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "Google play services connection failed");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Google play services connection suspended");
    }

    private void requestLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        }
    }

    @Subscribe
    public void startStopFetchingLocationData(PauseStatusChangedEvent pauseStatusChangedEvent) {
        boolean isOnPause = pauseStatusChangedEvent.isOnPause();

        if (isOnPause) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            Log.d(TAG, "Fetching task canceled ");
        } else {
            requestLocation();
            Log.d(TAG, "Fetching task started");
        }
    }
}
