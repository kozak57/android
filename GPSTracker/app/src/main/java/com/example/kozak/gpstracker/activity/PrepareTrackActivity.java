package com.example.kozak.gpstracker.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.example.kozak.gpstracker.R;
import com.example.kozak.gpstracker.dao.TrackDao;
import com.example.kozak.gpstracker.model.Track;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PrepareTrackActivity extends AppCompatActivity {
    public static final String TAG = "PrepareTrackActivity";
    @Bind(R.id.et_preparing_track_name)
    EditText etName;
    @Bind(R.id.et_preparing_track_desc)
    EditText etDesc;
    @Bind(R.id.et_preparing_track_category)
    EditText etCategory;

    private static final int MODE_EDIT = 0;
    private static final int MODE_CREATE = 1;
    private int currentMode;
    private long trackId;
    private Track track;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prepare_track_layout);
        ButterKnife.bind(this);
        Log.d(TAG, "OnCreate");

        Intent intent = getIntent();
        if (intent.hasExtra("id")) {
            Log.d(TAG, "Current mode: EDIT");
            currentMode = MODE_EDIT;
            trackId = intent.getLongExtra("id", 0L);
            track = TrackDao.getInstance().getTrack(trackId);
            Log.d(TAG, track.getName());
            etName.setText(track.getName(), TextView.BufferType.EDITABLE);
            etDesc.setText(track.getDescription(), TextView.BufferType.EDITABLE);
            etCategory.setText(track.getCategory(), TextView.BufferType.EDITABLE);

        } else {
            currentMode = MODE_CREATE;
            Log.d(TAG, "Current mode: CREATE");
        }
    }

    @OnClick(R.id.fab_create_track)
    void onClickCreateTrackButton() {
        if (this.isValid(etName)) {
            if (this.isValid(etDesc)) {
                if (this.isValid(etCategory)) {
                    startActivityIfNeeded(createOrUpdateTrack(), 0);
                    finish();
                } else etCategory.setError(getString(R.string.prepare_track_category_error));
            } else etDesc.setError(getString(R.string.prepare_track_desc_error));
        } else etName.setError(getString(R.string.prepare_track_name_error));
    }

    private Intent createOrUpdateTrack() {
        Intent intent;
        if (currentMode == MODE_CREATE) {
            intent = new Intent(this, RecordTrackActivity.class);
            intent.putExtra("name", etName.getText().toString());
            intent.putExtra("desc", etDesc.getText().toString());
            intent.putExtra("category", etCategory.getText().toString());

        } else {
            intent = new Intent(this, DetailedTrackActivity.class);
            intent.putExtra("id", trackId);
            track.setName(etName.getText().toString());
            track.setDescription(etDesc.getText().toString());
            track.setCategory(etCategory.getText().toString());

            TrackDao.getInstance().editTrack(trackId, track);
        }
        return intent;
    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        Log.d(TAG, "OnDestroy");

        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "OnResume");

    }

    @Override
    protected void onPause() {
        Log.d(TAG, "OnPause");

        super.onPause();
    }

    private boolean isValid(EditText editText) {
        boolean result = false;
        if (editText.getText().toString().length() > 0) {
            result = true;
            editText.setError(null);
        }
        return result;
    }
}