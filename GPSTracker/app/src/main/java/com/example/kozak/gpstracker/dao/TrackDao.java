package com.example.kozak.gpstracker.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.location.Location;
import android.util.Log;

import com.example.kozak.gpstracker.model.ContractClass;
import com.example.kozak.gpstracker.model.GPSData;
import com.example.kozak.gpstracker.model.Track;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TrackDao {
    public static final String TAG = "TrackDao";

    private TracksSQLiteHelper db;
    private static TrackDao instance;

    private TrackDao() {
        db = new TracksSQLiteHelper(TracksSQLiteHelper.DATABASE_NAME);
    }

    public TrackDao(TracksSQLiteHelper tracksSQLiteHelper) {
        db = tracksSQLiteHelper;
    }

    public static TrackDao getInstance() {
        if (instance == null) {
            synchronized (TrackDao.class) {
                if (instance == null) {
                    instance = new TrackDao();
                }
            }
        }
        return instance;
    }

    public long createTrack(Track track) {

        ContentValues values = new ContentValues();
        values.put(ContractClass.Tracks.COLUMN_NAME_NAME, track.getName());
        values.put(ContractClass.Tracks.COLUMN_NAME_DESC, track.getDescription());
        values.put(ContractClass.Tracks.COLUMN_NAME_CATEGORY, track.getCategory());
        values.put(ContractClass.Tracks.COLUMN_NAME_START_TIME, track.getStartTime().getTime());
        values.put(ContractClass.Tracks.COLUMN_NAME_STOP_TIME, track.getStopTime().getTime());

        return db.getWritableDatabase().insert(ContractClass.Tracks.TABLE_NAME, null, values);
    }

    public Track getTrack(long id) {
        String name;
        String description;
        String category;
        Date startTime;
        Date stopTime;
        Track track;
        List<GPSData> gpsDataList;
        double distance;

        Cursor cursor = db.getReadableDatabase().query(ContractClass.Tracks.TABLE_NAME,
                ContractClass.Tracks.DEFAULT_PROJECTION,
                "_ID = ?",
                new String[]{String.valueOf(id)},
                null, null, null);

        gpsDataList = getGPSDataByTrackId(id);
        if (cursor.moveToFirst()) {
            name = cursor.getString(cursor.getColumnIndex(ContractClass.Tracks.COLUMN_NAME_NAME));
            description = cursor.getString(cursor.getColumnIndex(ContractClass.Tracks.COLUMN_NAME_DESC));
            category = cursor.getString(cursor.getColumnIndex(ContractClass.Tracks.COLUMN_NAME_CATEGORY));
            startTime = new Date(cursor.getLong(cursor.getColumnIndex(ContractClass.Tracks.COLUMN_NAME_START_TIME)));
            stopTime = new Date(cursor.getLong(cursor.getColumnIndex(ContractClass.Tracks.COLUMN_NAME_STOP_TIME)));
            distance = cursor.getDouble(cursor.getColumnIndex(ContractClass.Tracks.COLUMN_NAME_DISTANCE));

            track = new Track(name, description, category, startTime, stopTime, distance, gpsDataList);

        } else {
            track = null;
        }
        cursor.close();
        return track;
    }

    public void editTrack(long trackId, Track track) {
        ContentValues values = new ContentValues();
        values.put(ContractClass.Tracks.COLUMN_NAME_NAME, track.getName());
        values.put(ContractClass.Tracks.COLUMN_NAME_DESC, track.getDescription());
        values.put(ContractClass.Tracks.COLUMN_NAME_CATEGORY, track.getCategory());
        values.put(ContractClass.Tracks.COLUMN_NAME_START_TIME, track.getStartTime().getTime());
        values.put(ContractClass.Tracks.COLUMN_NAME_STOP_TIME, track.getStopTime().getTime());
        values.put(ContractClass.Tracks.COLUMN_NAME_DISTANCE, track.getDistance());

        db.getWritableDatabase().update(
                ContractClass.Tracks.TABLE_NAME,
                values,
                ContractClass.Tracks._ID + "=?",
                new String[]{String.valueOf(trackId)});
    }

    public void deleteTrack(long id) {
        db.getWritableDatabase().delete(
                ContractClass.Tracks.TABLE_NAME,
                ContractClass.Tracks._ID + " = ?",
                new String[]{String.valueOf(id)}
        );

        db.getWritableDatabase().delete(
                ContractClass.GPSData.TABLE_NAME,
                ContractClass.GPSData.COLUMN_NAME_TRACK_ID_FK + " = ?",
                new String[]{String.valueOf(id)}
        );
    }

    public List<Track> getAllTracks() {

        List<Track> tracks = new ArrayList<>();
        Cursor cursor = getAllTracksInCursor();
        while (cursor.moveToNext()) {
            Track track = new Track();
            track.setName(cursor.getString(cursor.getColumnIndex(ContractClass.Tracks.COLUMN_NAME_NAME)));
            track.setDescription(cursor.getString(cursor.getColumnIndex(ContractClass.Tracks.COLUMN_NAME_DESC)));
            track.setCategory(cursor.getString(cursor.getColumnIndex(ContractClass.Tracks.COLUMN_NAME_CATEGORY)));
            track.setStartTime(new Date(cursor.getLong(cursor.getColumnIndex(ContractClass.Tracks.COLUMN_NAME_START_TIME))));
            track.setStopTime(new Date(cursor.getLong(cursor.getColumnIndex(ContractClass.Tracks.COLUMN_NAME_STOP_TIME))));
            track.setDistance(cursor.getDouble(cursor.getColumnIndex(ContractClass.Tracks.COLUMN_NAME_DISTANCE)));
            track.setGpsDataList(getGPSDataByTrackId(cursor.getInt(cursor.getColumnIndex(ContractClass.Tracks._ID))));

            tracks.add(track);
        }
        cursor.close();
        return tracks;
    }

    public Cursor getAllTracksInCursor() {
        String query = "SELECT * FROM " + ContractClass.Tracks.TABLE_NAME;
        return db.getWritableDatabase().rawQuery(query, null);
    }

    public int getTrackCount() {
        return getAllTracksInCursor().getCount();
    }

    public double getTotalDistance() {
        double result = 0;
        String query = "SELECT SUM (" + ContractClass.Tracks.COLUMN_NAME_DISTANCE + ") FROM " +
                ContractClass.Tracks.TABLE_NAME + ";";
        Cursor cursor = db.getReadableDatabase().rawQuery(query, null);

        if (cursor.moveToFirst()) {
            result = cursor.getDouble(0);
        }
        cursor.close();

        return result;
    }

    public double getTrackDistance(long id) {

        Track track = getTrack(id);
        double distance = track.getDistance();
        double result;

        List<GPSData> gpsDataList = track.getGpsDataList();
        float[] resultArray = new float[1];

        if (distance > 0.0) {
            result = distance;
        } else {
            if (gpsDataList.size() > 1) {
                result = 0.0;

                for (int i = 0; i < gpsDataList.size() - 1; i++) {
                    GPSData oldGpsData = gpsDataList.get(i);
                    GPSData newGpsData = gpsDataList.get(i + 1);
                    Location.distanceBetween(oldGpsData.getLatitude(), oldGpsData.getLongitude(), newGpsData.getLatitude(), newGpsData.getLongitude(), resultArray);

                    result += resultArray[0];
                }

                track.setDistance(result);
                editTrack(id, track);
            } else result = 0.0;
        }
        return result;
    }

    public long getTotalTime() {
        String query = "SELECT " + ContractClass.Tracks._ID + " FROM " + ContractClass.Tracks.TABLE_NAME;
        Cursor cursor = db.getReadableDatabase().rawQuery(query, null);
        long totalTime = 0L;
        if (cursor.moveToFirst()) {
            do {
                totalTime += getTrackTime(cursor.getLong(cursor.getColumnIndex(ContractClass.Tracks._ID)));
            } while (cursor.moveToNext());
        }

        cursor.close();
        return totalTime;
    }

    public long getTrackTime(long id) {

        Track track = getTrack(id);
        long startTime = track.getStartTime().getTime();
        long finishTime = track.getStopTime().getTime();
        List<GPSData> gpsDataList = track.getGpsDataList();
        if (gpsDataList.size() > 1) {
            startTime = gpsDataList.get(0).getDate().getTime();
            finishTime = gpsDataList.get(gpsDataList.size() - 1).getDate().getTime();
        }
        return finishTime - startTime;
    }

    public double getTrackAverageSpeed(long id) {

        double result = 0;
        String query = "SELECT AVG (" + ContractClass.GPSData.COLUMN_NAME_SPEED + ") FROM " +
                ContractClass.GPSData.TABLE_NAME + " WHERE " + ContractClass.GPSData._ID + " = ?;";

        Cursor cursor = db.getReadableDatabase().rawQuery(query, new String[]{String.valueOf(id)});

        if (cursor.moveToFirst()) {
            result = cursor.getDouble(0);
        }
        cursor.close();

        return result;

    }

    public long createGpsData(GPSData gpsData) {
        ContentValues values = new ContentValues();
        values.put(ContractClass.GPSData.COLUMN_NAME_LONGITUDE, gpsData.getLongitude());
        values.put(ContractClass.GPSData.COLUMN_NAME_LATITUDE, gpsData.getLatitude());
        values.put(ContractClass.GPSData.COLUMN_NAME_ALTITUDE, gpsData.getAltitude());
        values.put(ContractClass.GPSData.COLUMN_NAME_SPEED, gpsData.getSpeed());
        values.put(ContractClass.GPSData.COLUMN_NAME_TIME, gpsData.getDate().getTime());
        values.put(ContractClass.GPSData.COLUMN_NAME_ACCURACY, gpsData.getAccuracy());
        values.put(ContractClass.GPSData.COLUMN_NAME_BEARING, gpsData.getBearing());
        values.put(ContractClass.GPSData.COLUMN_NAME_TRACK_ID_FK, gpsData.getTrackId());

        return db.getWritableDatabase().insert(ContractClass.GPSData.TABLE_NAME, null, values);
    }

    public List<GPSData> getGPSDataByTrackId(long trackId) {
        List<GPSData> gpsDataList = new ArrayList<>();
        String query = "SELECT * FROM " + ContractClass.GPSData.TABLE_NAME + " WHERE " + ContractClass.GPSData.COLUMN_NAME_TRACK_ID_FK + " = ?;";
        Log.d(TAG, "Query is " + query);

        Cursor cursor = db.getReadableDatabase().rawQuery(query, new String[]{String.valueOf(trackId)});

        if (cursor.moveToFirst()) {
            do {
                GPSData gpsData = new GPSData();
                gpsData.setLongitude(cursor.getDouble(cursor.getColumnIndex(ContractClass.GPSData.COLUMN_NAME_LONGITUDE)));
                gpsData.setLatitude(cursor.getDouble(cursor.getColumnIndex(ContractClass.GPSData.COLUMN_NAME_LATITUDE)));
                gpsData.setAltitude(cursor.getDouble(cursor.getColumnIndex(ContractClass.GPSData.COLUMN_NAME_ALTITUDE)));
                gpsData.setSpeed(cursor.getDouble(cursor.getColumnIndex(ContractClass.GPSData.COLUMN_NAME_SPEED)));
                gpsData.setDate(new Date(cursor.getLong(cursor.getColumnIndex(ContractClass.GPSData.COLUMN_NAME_TIME))));
                gpsData.setAccuracy(cursor.getDouble(cursor.getColumnIndex(ContractClass.GPSData.COLUMN_NAME_ACCURACY)));
                gpsData.setBearing(cursor.getDouble(cursor.getColumnIndex(ContractClass.GPSData.COLUMN_NAME_BEARING)));
                gpsData.setTrackId(trackId);

                gpsDataList.add(gpsData);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return gpsDataList;
    }

}
