package com.example.kozak.gpstracker.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kozak.gpstracker.R;
import com.example.kozak.gpstracker.dao.TrackDao;
import com.example.kozak.gpstracker.model.Track;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class DetailedTrackActivity extends AppCompatActivity {

    @Bind(R.id.tv_detailed_name)
    TextView name;
    @Bind(R.id.tv_detailed_description)
    TextView description;
    @Bind(R.id.tv_detailed_category)
    TextView category;
    @Bind(R.id.tv_detailed_distance)
    TextView distance;
    @Bind(R.id.tv_detailed_start_time)
    TextView startTime;
    @Bind(R.id.tv_detailed_stop_time)
    TextView stopTime;
    @Bind(R.id.tv_detailed_average_speed)
    TextView speed;

    private long trackId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.track_detailed_layout);
        ButterKnife.bind(this);

        trackId = getIntent().getLongExtra("id", 0L);
        Track track = TrackDao.getInstance().getTrack(trackId);

        name.setText(track.getName());
        description.setText(track.getDescription());
        category.setText(track.getCategory());
        distance.setText(String.valueOf(TrackDao.getInstance().getTrackDistance(trackId)));
        //TODO format time in readable format
        startTime.setText((track.getStartTime()).toString());
        stopTime.setText((track.getStopTime().toString()));
        speed.setText(String.valueOf(TrackDao.getInstance().getTrackAverageSpeed(trackId)));
    }

    @Override
    public void onBackPressed() {
        startActivityIfNeeded(new Intent(this, TrackListActivity.class), 0);
        finish();
    }

    @OnClick(R.id.bt_edit_track)
    public void onEditClick() {
        Intent intent = new Intent(this, PrepareTrackActivity.class);
        intent.putExtra("id", trackId);
        startActivityIfNeeded(intent, 0);
        finish();
    }

    @OnClick(R.id.bt_delete_track)
    public void onDeleteClick() {
        Intent intent = getIntent();
        TrackDao.getInstance().deleteTrack(intent.getLongExtra("id", 0));
        Toast.makeText(this, "Track deleted successfully", Toast.LENGTH_SHORT).show();

        finish();
    }

    @OnClick(R.id.bt_view_on_map)
    public void onViewClick() {
        Toast.makeText(this, "Viewing track on map will be soon", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }
}
