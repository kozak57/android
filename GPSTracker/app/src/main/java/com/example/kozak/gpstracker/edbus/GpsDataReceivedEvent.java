package com.example.kozak.gpstracker.edbus;

import android.location.Location;

public class GpsDataReceivedEvent {

    private Location location;

    public GpsDataReceivedEvent(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }
}
