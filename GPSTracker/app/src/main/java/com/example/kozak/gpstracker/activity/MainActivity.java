package com.example.kozak.gpstracker.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.kozak.gpstracker.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {


    //TODO make statistics

    public static final String TAG = "MainActivity";

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
//        Intent intent = new Intent(this, RecordTrackActivity.class);
//        intent.putExtra("name", "Test1");
//        intent.putExtra("desc", "Test1");
//        intent.putExtra("category", "Test1");
//        startActivity(intent);
        setSupportActionBar(toolbar);
    }

    @OnClick(R.id.bt_track_list)
    void onClickTrackList() {
        startActivity(new Intent(getBaseContext(), TrackListActivity.class));
    }

    @OnClick(R.id.fab_prepare_track)
    void onClickCreateNewTrack() {
        startActivity(new Intent(getBaseContext(), PrepareTrackActivity.class));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }
}
