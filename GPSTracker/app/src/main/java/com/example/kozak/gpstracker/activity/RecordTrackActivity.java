package com.example.kozak.gpstracker.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.example.kozak.gpstracker.R;
import com.example.kozak.gpstracker.RecordingService;
import com.example.kozak.gpstracker.dao.TrackDao;
import com.example.kozak.gpstracker.edbus.EDBus;
import com.example.kozak.gpstracker.edbus.GpsDataReceivedEvent;
import com.example.kozak.gpstracker.edbus.PauseStatusChangedEvent;
import com.example.kozak.gpstracker.model.GPSData;
import com.example.kozak.gpstracker.model.Track;
import com.example.kozak.gpstracker.util.StringFormatter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RecordTrackActivity extends AppCompatActivity {
    private static final int LOCATION_PERMISSIONS = 0;
    @Bind(R.id.tv_record_track_coordinates)
    TextView tvCoordinates;
    @Bind(R.id.tv_record_track_speed)
    TextView tvSpeed;
    @Bind(R.id.tv_record_track_mode)
    TextView tvMode;
    @Bind(R.id.tv_record_track_time)
    TextView tvTime;
    @Bind(R.id.tv_record_track_distance)
    TextView tvDistance;
    @Bind(R.id.bt_record_track_start_pause)
    Button pauseButton;
    @Bind(R.id.bt_record_track_stop)
    Button stopButton;

    public static final String TAG = "RecordTrackActivity";
    private static boolean isOnPause = false;
    private Intent service;
    private long trackId;
    private Track track;
    private GPSData gpsData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.track_recording_layout);
        ButterKnife.bind(this);

        if (checkPermission()) {
            startService();
        }
    }

    @Subscribe
    public void recordGpsData(GpsDataReceivedEvent gpsDataReceivedEvent) {
        Location location = gpsDataReceivedEvent.getLocation();
        long elapsedTime;
        double distance;
        pauseButton.setEnabled(true);

        gpsData = new GPSData(trackId, location);
        elapsedTime = gpsData.getDate().getTime() - track.getStartTime().getTime();
        String coordinates = String.valueOf(location.getLatitude() + "/" + String.valueOf(location.getLongitude()));

        TrackDao.getInstance().createGpsData(gpsData);
        distance = TrackDao.getInstance().getTrackDistance(trackId);

        tvCoordinates.setText(coordinates);
        tvSpeed.setText(StringFormatter.getRoundedDouble((location.getSpeed() * 3.6), 2));
        tvMode.setText(location.getProvider());
        tvTime.setText(StringFormatter.getTimePeriod(elapsedTime));
        tvDistance.setText(StringFormatter.getRoundedDouble(distance, 2));
    }

    @Override
    protected void onResume() {
        super.onResume();
        EDBus.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "record track activity on pause");
        EDBus.getInstance().unregister(this);
        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSIONS: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "Permission Granted: " + permissions[i]);
                        if (testPlayServices()) {
                            startService();
                            pauseButton.setEnabled(true);
                        }
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    private void startService() {
        Intent intent = getIntent();
        track = new Track(intent.getStringExtra("name"), intent.getStringExtra("desc"), intent.getStringExtra("category"));

        service = new Intent(getBaseContext(), RecordingService.class);
        trackId = TrackDao.getInstance().createTrack(track);

        startService(service);
        EDBus.getInstance().post(new PauseStatusChangedEvent(isOnPause));
    }

    private boolean checkPermission() {

        boolean result = false;

        int hasLocationPermission;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            hasLocationPermission = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);

            List<String> permissions = new ArrayList<>();
            if (hasLocationPermission == PackageManager.PERMISSION_DENIED) {
                permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
            } else result = true;

            if (!permissions.isEmpty()) {
                requestPermissions(permissions.toArray(new String[permissions.size()]), LOCATION_PERMISSIONS);
            }
        } else result = true;

        return result;
    }

    private boolean testPlayServices() {
        int checkGooglePlayServices = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if (checkGooglePlayServices != ConnectionResult.SUCCESS) {
            Log.d(TAG, "Google play services does not available");
            GoogleApiAvailability.getInstance().getErrorDialog(this, checkGooglePlayServices, 1122).show();
            return false;
        }
        Log.d(TAG, "Google play services available");
        return true;
    }

    @OnClick(R.id.bt_record_track_start_pause)
    public void onPauseClick() {
        isOnPause = !isOnPause;

        if (isOnPause) {
            pauseButton.setText(R.string.gps_recording_continue);
        } else {
            pauseButton.setText(R.string.gps_recording_pause);
        }
        EDBus.getInstance().post(new PauseStatusChangedEvent(isOnPause));
    }

    @OnClick(R.id.bt_record_track_stop)
    public void onStopClick() {
        stopService(service);
        Intent intent = new Intent(getBaseContext(), DetailedTrackActivity.class);
        intent.putExtra("id", trackId);

        track.setStopTime(gpsData.getDate());
        TrackDao.getInstance().editTrack(trackId, track);

        startActivityIfNeeded(intent, 0);
        finish();
    }
}
