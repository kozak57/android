package com.example.kozak.gpstracker.util;


import android.support.annotation.NonNull;
import android.util.Log;

import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

public class StringFormatter {
    public static final String TAG = "TomeFormatter";

    public static String getTimePeriod(long time) {

        Period period = new Period(time);

        PeriodFormatter periodFormatter = new PeriodFormatterBuilder()
//                .printZeroNever().appendYears().appendSuffix(" year", " years").appendSeparator(", ")
//                .printZeroNever().appendMonths().appendSuffix(" month", " months").appendSeparator(", ")
//                .printZeroNever().appendDays().appendSuffix(" day", " days").appendSeparator(", ")
                .printZeroNever().appendHours().appendSuffix(" hour", " hours").appendSeparator(", ")
                .printZeroNever().appendMinutes().appendSuffix(" minute", " minutes").appendSeparator(", ")
                .printZeroNever().appendSeconds().appendSuffix(" second", " seconds")
                .toFormatter();

        Log.d(TAG, "Converted from " + time + ", to " + period.toString(periodFormatter));
        return period.toString(periodFormatter);
    }

    @NonNull
    public static String getRoundedDouble(double input, int digits) {
        String result;
        double digitsAfterDot = 1.0;
        if (digits > 0) {
            for (int i = 0; i < digits; i++) {
                digitsAfterDot = digitsAfterDot * 10;
            }
            result = String.valueOf(Math.round(input * digitsAfterDot) / digitsAfterDot);
        } else if (digits == 0) {
            result = String.valueOf(Math.round(input));
        } else {
            result = "0";
        }
        return result;
    }
}
