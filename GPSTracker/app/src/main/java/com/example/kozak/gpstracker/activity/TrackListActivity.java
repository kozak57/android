package com.example.kozak.gpstracker.activity;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.kozak.gpstracker.NewTrackAdapter;
import com.example.kozak.gpstracker.R;
import com.example.kozak.gpstracker.TrackAdapter;
import com.example.kozak.gpstracker.dao.TrackDao;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TrackListActivity extends AppCompatActivity {
    public static final String TAG = "track List Activity";

    @Bind(R.id.recycler_track_view)
    RecyclerView recyclerView;
    private NewTrackAdapter newAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.track_list_layout);
        ButterKnife.bind(this);
        Log.d(TAG, "OnCreate");
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "OnDestroy");
        ButterKnife.unbind(this);
        ButterKnife.unbind(TrackAdapter.class);
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "OnResume");

        newAdapter = new NewTrackAdapter(TrackDao.getInstance().getAllTracksInCursor(), getBaseContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        recyclerView.setAdapter(newAdapter);
        newAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "OnPause");
    }
}
