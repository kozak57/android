package com.example.kozak.gpstracker.model;

import android.provider.BaseColumns;

public final class ContractClass {
    public ContractClass() {
    }

    public static final class Tracks implements BaseColumns {
        public static final String TABLE_NAME = "tracks";
        public static final String PATH_TRACKS = "/tracks";
        public static final String PATH_TRACKS_ID = "/tracks/";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_DESC = "desc";
        public static final String COLUMN_NAME_CATEGORY = "category";
        public static final String COLUMN_NAME_START_TIME = "start_time";
        public static final String COLUMN_NAME_STOP_TIME = "stop_time";
        public static final String COLUMN_NAME_DISTANCE = "distance";
        public static final String DEFAULT_SORT_ORDER = COLUMN_NAME_NAME + " ASC";
        public static final String[] DEFAULT_PROJECTION = new String[]{
                Tracks._ID,
                Tracks.COLUMN_NAME_NAME,
                Tracks.COLUMN_NAME_DESC,
                Tracks.COLUMN_NAME_CATEGORY,
                Tracks.COLUMN_NAME_START_TIME,
                Tracks.COLUMN_NAME_STOP_TIME,
                Tracks.COLUMN_NAME_DISTANCE,
        };
    }

    public static final class GPSData implements BaseColumns {
        public static final String TABLE_NAME = "GPSData";
        public static final String PATH_GPS_DATA = "/GPSData";
        public static final String PATH_GPS_DATA_ID = "/GPSData/";
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        public static final String COLUMN_NAME_ALTITUDE = "altitude";
        public static final String COLUMN_NAME_SPEED = "speed";
        public static final String COLUMN_NAME_TIME = "time";
        public static final String COLUMN_NAME_ACCURACY = "accuracy";
        public static final String COLUMN_NAME_BEARING = "bearing";
        public static final String COLUMN_NAME_TRACK_ID_FK = "track_id_fk";

        public static final String DEFAULT_SORT_ORDER = COLUMN_NAME_TIME + " ASC";
        public static final String[] DEFAULT_PROJECTION = new String[]{
                GPSData._ID,
                GPSData.COLUMN_NAME_LONGITUDE,
                GPSData.COLUMN_NAME_LATITUDE,
                GPSData.COLUMN_NAME_ALTITUDE,
                GPSData.COLUMN_NAME_SPEED,
                GPSData.COLUMN_NAME_ACCURACY,
                GPSData.COLUMN_NAME_TIME,
                GPSData.COLUMN_NAME_BEARING,
                GPSData.COLUMN_NAME_TRACK_ID_FK
        };
    }
}

