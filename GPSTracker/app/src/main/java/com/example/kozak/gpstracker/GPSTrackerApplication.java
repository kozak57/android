package com.example.kozak.gpstracker;

import android.app.Application;
import android.content.Context;

import net.danlew.android.joda.JodaTimeAndroid;

public class GPSTrackerApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        GPSTrackerApplication.context = getApplicationContext();
        JodaTimeAndroid.init(this);
    }

    public static Context getAppContext() {
        return GPSTrackerApplication.context;
    }
}
