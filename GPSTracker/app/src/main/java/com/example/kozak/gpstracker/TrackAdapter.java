package com.example.kozak.gpstracker;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.kozak.gpstracker.model.ContractClass;

import butterknife.Bind;
import butterknife.ButterKnife;


public class TrackAdapter extends CursorAdapter {


    public TrackAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.track_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        view.setTag(viewHolder);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView name;
        TextView distance;

        ViewHolder viewHolder = (ViewHolder) view.getTag();

        name = viewHolder.getName();
        distance = viewHolder.getDistance();

        name.setText(cursor.getString(cursor.getColumnIndex(ContractClass.Tracks.COLUMN_NAME_NAME)));
        distance.setText(String.valueOf(cursor.getDouble(cursor.getColumnIndex(ContractClass.Tracks.COLUMN_NAME_DISTANCE))));

    }

    static class ViewHolder {
        @Bind(R.id.tv_detailed_name)
        TextView name;
        @Bind(R.id.tv_detailed_distance)
        TextView distance;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        public TextView getName() {
            return name;
        }

        public TextView getDistance() {
            return distance;
        }
    }
}