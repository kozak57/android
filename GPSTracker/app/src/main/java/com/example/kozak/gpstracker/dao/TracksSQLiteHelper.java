package com.example.kozak.gpstracker.dao;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.kozak.gpstracker.GPSTrackerApplication;
import com.example.kozak.gpstracker.model.ContractClass;

public class TracksSQLiteHelper extends SQLiteOpenHelper {
    private static final String TAG = "TracksSQLiteHelper";

    public static final int DATABASE_VERSION = 4;
    public static final String DATABASE_NAME = "Tracks.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String INT_TYPE = " INTEGER";
    private static final String DOUBLE_TYPE = " REAL";

    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_TRACKS_TABLE =
            "CREATE TABLE " + ContractClass.Tracks.TABLE_NAME + " (" +
                    ContractClass.Tracks._ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
                    ContractClass.Tracks.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    ContractClass.Tracks.COLUMN_NAME_DESC + TEXT_TYPE + COMMA_SEP +
                    ContractClass.Tracks.COLUMN_NAME_CATEGORY + TEXT_TYPE + COMMA_SEP +
                    ContractClass.Tracks.COLUMN_NAME_START_TIME + INT_TYPE + COMMA_SEP +
                    ContractClass.Tracks.COLUMN_NAME_STOP_TIME + INT_TYPE + COMMA_SEP +
                    ContractClass.Tracks.COLUMN_NAME_DISTANCE + DOUBLE_TYPE +
                    " )";

    private static final String SQL_CREATE_GPS_DATA_TABLE =
            "CREATE TABLE " + ContractClass.GPSData.TABLE_NAME + " (" +
                    ContractClass.GPSData._ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + COMMA_SEP +
                    ContractClass.GPSData.COLUMN_NAME_LONGITUDE + DOUBLE_TYPE + COMMA_SEP +
                    ContractClass.GPSData.COLUMN_NAME_LATITUDE + DOUBLE_TYPE + COMMA_SEP +
                    ContractClass.GPSData.COLUMN_NAME_ALTITUDE + DOUBLE_TYPE + COMMA_SEP +
                    ContractClass.GPSData.COLUMN_NAME_SPEED + DOUBLE_TYPE + COMMA_SEP +
                    ContractClass.GPSData.COLUMN_NAME_ACCURACY + DOUBLE_TYPE + COMMA_SEP +
                    ContractClass.GPSData.COLUMN_NAME_TIME + INT_TYPE + COMMA_SEP +
                    ContractClass.GPSData.COLUMN_NAME_BEARING + DOUBLE_TYPE + COMMA_SEP +
                    ContractClass.GPSData.COLUMN_NAME_TRACK_ID_FK + INT_TYPE + COMMA_SEP +
                    "FOREIGN KEY (" + ContractClass.GPSData.COLUMN_NAME_TRACK_ID_FK +
                    ") REFERENCES " + ContractClass.Tracks.TABLE_NAME + " ( " + ContractClass.Tracks._ID + ")" +
                    " )";

    public TracksSQLiteHelper(String dbName) {
        super(GPSTrackerApplication.getAppContext(), dbName, null, DATABASE_VERSION);
    }

    private void fillInitialData(SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(ContractClass.Tracks.COLUMN_NAME_NAME, "1 track");
        values.put(ContractClass.Tracks.COLUMN_NAME_DESC, "1 track description");
        values.put(ContractClass.Tracks.COLUMN_NAME_CATEGORY, "hiking");
        values.put(ContractClass.Tracks.COLUMN_NAME_START_TIME, 1448470436767L);
        values.put(ContractClass.Tracks.COLUMN_NAME_STOP_TIME, 1448470336767L);
        values.put(ContractClass.Tracks.COLUMN_NAME_DISTANCE, 0.0);
        db.insert(ContractClass.Tracks.TABLE_NAME, null, values);

        values.clear();

        values.put(ContractClass.Tracks.COLUMN_NAME_NAME, "2 track");
        values.put(ContractClass.Tracks.COLUMN_NAME_DESC, "2 track description");
        values.put(ContractClass.Tracks.COLUMN_NAME_CATEGORY, "hiking");
        values.put(ContractClass.Tracks.COLUMN_NAME_START_TIME, 1448470435767L);
        values.put(ContractClass.Tracks.COLUMN_NAME_STOP_TIME, 1448470335767L);
        values.put(ContractClass.Tracks.COLUMN_NAME_DISTANCE, 0.0);
        db.insert(ContractClass.Tracks.TABLE_NAME, null, values);

        values.clear();

        values.put(ContractClass.GPSData.COLUMN_NAME_LONGITUDE, 48.461842);
        values.put(ContractClass.GPSData.COLUMN_NAME_LATITUDE, 35.032379);
        values.put(ContractClass.GPSData.COLUMN_NAME_ALTITUDE, 25.3);
        values.put(ContractClass.GPSData.COLUMN_NAME_SPEED, 5.032379);
        values.put(ContractClass.GPSData.COLUMN_NAME_ACCURACY, 0.0);
        values.put(ContractClass.GPSData.COLUMN_NAME_BEARING, 35.0);
        values.put(ContractClass.GPSData.COLUMN_NAME_TIME, 1448470436767L);
        values.put(ContractClass.GPSData.COLUMN_NAME_TRACK_ID_FK, 1);
        db.insert(ContractClass.GPSData.TABLE_NAME, null, values);

        values.clear();
        values.put(ContractClass.GPSData.COLUMN_NAME_LONGITUDE, 48.440842);
        values.put(ContractClass.GPSData.COLUMN_NAME_LATITUDE, 35.032379);
        values.put(ContractClass.GPSData.COLUMN_NAME_ALTITUDE, 25.3);
        values.put(ContractClass.GPSData.COLUMN_NAME_SPEED, 5.032379);
        values.put(ContractClass.GPSData.COLUMN_NAME_ACCURACY, 0.0);
        values.put(ContractClass.GPSData.COLUMN_NAME_BEARING, 35.0);
        values.put(ContractClass.GPSData.COLUMN_NAME_TIME, 1448470436767L);
        values.put(ContractClass.GPSData.COLUMN_NAME_TRACK_ID_FK, 1);
        db.insert(ContractClass.GPSData.TABLE_NAME, null, values);

        values.clear();
        values.put(ContractClass.GPSData.COLUMN_NAME_LONGITUDE, 48.460842);
        values.put(ContractClass.GPSData.COLUMN_NAME_LATITUDE, 35.042379);
        values.put(ContractClass.GPSData.COLUMN_NAME_ALTITUDE, 25.3);
        values.put(ContractClass.GPSData.COLUMN_NAME_SPEED, 5.032379);
        values.put(ContractClass.GPSData.COLUMN_NAME_ACCURACY, 0.0);
        values.put(ContractClass.GPSData.COLUMN_NAME_BEARING, 35.0);
        values.put(ContractClass.GPSData.COLUMN_NAME_TIME, 1448470436767L);
        values.put(ContractClass.GPSData.COLUMN_NAME_TRACK_ID_FK, 1);
        db.insert(ContractClass.GPSData.TABLE_NAME, null, values);

        values.clear();

        values.put(ContractClass.GPSData.COLUMN_NAME_LONGITUDE, 48.460852);
        values.put(ContractClass.GPSData.COLUMN_NAME_LATITUDE, 35.032359);
        values.put(ContractClass.GPSData.COLUMN_NAME_ACCURACY, 0.0);
        values.put(ContractClass.GPSData.COLUMN_NAME_BEARING, 35.0);
        values.put(ContractClass.GPSData.COLUMN_NAME_TIME, 1448470446767L);
        values.put(ContractClass.GPSData.COLUMN_NAME_TRACK_ID_FK, 2);
        db.insert(ContractClass.GPSData.TABLE_NAME, null, values);

        values.clear();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_GPS_DATA_TABLE);
        db.execSQL(SQL_CREATE_TRACKS_TABLE);
        Log.d(TAG, "Database created");
        fillInitialData(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ContractClass.GPSData.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ContractClass.Tracks.TABLE_NAME);
        onCreate(db);
    }
}
