package com.example.kozak.gpstracker;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kozak.gpstracker.activity.DetailedTrackActivity;
import com.example.kozak.gpstracker.model.ContractClass;
import com.example.kozak.gpstracker.util.StringFormatter;

public class NewTrackAdapter extends RecyclerView.Adapter<NewTrackAdapter.ViewHolder> {
    private static Context context;
    private Cursor cursor;
    public static final String TAG = "NewTrackAdapter";

    public NewTrackAdapter(Cursor cursor, Context baseContext) {
        this.cursor = cursor;
        context = baseContext;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView distance;
        private long trackId;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tv_detailed_name);
            distance = (TextView) itemView.findViewById(R.id.tv_detailed_distance);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, DetailedTrackActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("id", trackId);
                    context.startActivity(intent);
                    Log.d(TAG, "Item #" + trackId + " was clicked");
                }
            });

            Log.d(TAG, "ViewHolder created");
        }

        public TextView getName() {
            return name;
        }

        public TextView getDistance() {
            return distance;
        }

        public void setTrackId(long trackId) {
            this.trackId = trackId;
        }

        public long getTrackId() {
            return trackId;
        }
    }

    @Override
    public NewTrackAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.track_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewTrackAdapter.ViewHolder holder, int position) {
        cursor.moveToPosition(position);

        holder.getName().setText(cursor.getString(cursor.getColumnIndex(ContractClass.Tracks.COLUMN_NAME_NAME)));
        holder.getDistance().setText(StringFormatter.getRoundedDouble(cursor.getDouble(cursor.getColumnIndex(ContractClass.Tracks.COLUMN_NAME_DISTANCE)), 2));
        holder.setTrackId(cursor.getLong(cursor.getColumnIndex(ContractClass.Tracks._ID)));
        Log.d(TAG, "ViewHolder binded");
    }

    @Override
    public int getItemCount() {
        return cursor.getCount();
    }
}
