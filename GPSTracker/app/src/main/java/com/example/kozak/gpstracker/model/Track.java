package com.example.kozak.gpstracker.model;

import android.provider.BaseColumns;

import java.util.Date;
import java.util.List;

public class Track implements BaseColumns{
    private String name;
    private String description;
    private String category;
    private Date startTime;
    private Date stopTime;
    private double distance;
    private List<GPSData> gpsDataList;

    public Track() {
    }

    public Track(String name, String description, String category) {
        this.name = name;
        this.description = description;
        this.category = category;
        this.startTime = new Date();
        this.stopTime = new Date();
        this.distance = 0.0;
    }

    public Track(String name, String description, String category, Date startTime, Date stopTime, double distance, List<GPSData> gpsDataList) {
        this.name = name;
        this.description = description;
        this.category = category;
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.distance = distance;
        this.gpsDataList = gpsDataList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getStopTime() {
        return stopTime;
    }

    public void setStopTime(Date stopTime) {
        this.stopTime = stopTime;
    }

    public List<GPSData> getGpsDataList() {
        return gpsDataList;
    }

    public void setGpsDataList(List<GPSData> gpsDataList) {
        this.gpsDataList = gpsDataList;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
