package com.example.kozak.gpstracker.edbus;

public class PauseStatusChangedEvent {

    private boolean isOnPause;

    public PauseStatusChangedEvent(boolean isOnPause) {
        this.isOnPause = isOnPause;
    }

    public boolean isOnPause() {
        return isOnPause;
    }
}
