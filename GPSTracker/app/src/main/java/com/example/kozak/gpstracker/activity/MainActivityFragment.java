package com.example.kozak.gpstracker.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kozak.gpstracker.R;
import com.example.kozak.gpstracker.dao.TrackDao;
import com.example.kozak.gpstracker.util.StringFormatter;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivityFragment extends Fragment {

    public MainActivityFragment() {
    }

    public static final String TAG = "mainFragment";

    @Bind(R.id.tv_total_distance)
    TextView totalDistance;
    @Bind(R.id.tv_total_time)
    TextView totalTime;
    @Bind(R.id.tv_total_tracks)
    TextView totalTracks;

    private View view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        long time = TrackDao.getInstance().getTotalTime();
        int tracksCount = TrackDao.getInstance().getTrackCount();
        totalTracks.setText(String.valueOf(tracksCount));
        totalTime.setText(StringFormatter.getTimePeriod(time));
        totalDistance.setText(String.valueOf(StringFormatter.getRoundedDouble(TrackDao.getInstance().getTotalDistance(), 2)));

    }

    @Override
    public void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }
}
