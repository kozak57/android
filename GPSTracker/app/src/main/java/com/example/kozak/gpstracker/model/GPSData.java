package com.example.kozak.gpstracker.model;

import android.location.Location;
import android.provider.BaseColumns;

import java.util.Date;

public class GPSData implements BaseColumns {
    private double longitude;
    private double latitude;
    private double altitude;
    private double speed;
    private Date date;
    private double accuracy;
    private double bearing;
    private long trackId;

    public GPSData() {
    }

    public GPSData(long trackId, Location location) {
        this.trackId = trackId;
        this.longitude = location.getLongitude();
        this.latitude = location.getLatitude();
        this.altitude = location.getAltitude();
        this.speed = location.getSpeed();
        this.date = new Date(location.getTime());
        this.accuracy = location.getAccuracy();
        this.bearing = location.getBearing();
        this.trackId = trackId;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    public double getBearing() {
        return bearing;
    }

    public void setBearing(double bearing) {
        this.bearing = bearing;
    }

    public long getTrackId() {
        return trackId;
    }

    public void setTrackId(long trackId) {
        this.trackId = trackId;
    }
}
