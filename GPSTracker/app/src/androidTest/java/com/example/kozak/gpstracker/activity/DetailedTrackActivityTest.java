package com.example.kozak.gpstracker.activity;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.kozak.gpstracker.R;
import com.example.kozak.gpstracker.dao.TrackDao;
import com.example.kozak.gpstracker.model.Track;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isClickable;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class DetailedTrackActivityTest {
    private static final String TEST_NAME = "detailedTest name";
    private static final String TEST_DESC = "detailedTest description";
    private static final String TEST_CATEGORY = "detailedTest category";
    private static final String TEST_NAME_EDITED = "Edited detailedTest name";
    private static final String TEST_DESC_EDITED = "Edited detailedTest description";
    private static final String TEST_CATEGORY_EDITED = "Edited detailedTest category";
    private static final Date TEST_START_TIME = new Date();
    private static final Date TEST_STOP_TIME = new Date(TEST_START_TIME.getTime() + 5000);
    private long trackId;


    @Rule
    public ActivityTestRule<DetailedTrackActivity> activityTestRule = new ActivityTestRule<DetailedTrackActivity>(DetailedTrackActivity.class) {
        @Override
        protected Intent getActivityIntent() {

            trackId = createTrack();
            Context targetContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
            Intent result = new Intent(targetContext, DetailedTrackActivity.class);
            result.putExtra("id", trackId);
            return result;
        }
    };

    @Before
    public void preparingBeforeTests() {
        // trackId = createTrack();
    }

    @Test
    public void checkActivity() {
        onView(withId(R.id.tv_detailed_name)).check(matches(isDisplayed())).check(matches(isDisplayed())).check(matches(withText(TEST_NAME)));
        onView(withId(R.id.tv_detailed_description)).check(matches(isDisplayed())).check(matches(isDisplayed())).check(matches(withText(TEST_DESC)));
        onView(withId(R.id.tv_detailed_category)).check(matches(isDisplayed())).check(matches(isDisplayed())).check(matches(withText(TEST_CATEGORY)));

        onView(withId(R.id.bt_delete_track)).check(matches(isDisplayed())).check(matches(isClickable()));
        onView(withId(R.id.bt_edit_track)).check(matches(isDisplayed())).check(matches(isClickable()));
        onView(withId(R.id.bt_view_on_map)).check(matches(isDisplayed())).check(matches(isClickable()));
    }

    @Test
    public void buttonEditTest() {
        onView(withId(R.id.bt_edit_track)).perform(click());

        onView(withId(R.id.et_preparing_track_name)).perform(clearText(), typeText(TEST_NAME_EDITED));
        onView(withId(R.id.et_preparing_track_desc)).perform(clearText(), typeText(TEST_DESC_EDITED));
        onView(withId(R.id.et_preparing_track_category)).perform(clearText(), typeText(TEST_CATEGORY_EDITED)).perform(closeSoftKeyboard());

        onView(withId(R.id.fab_create_track)).perform(click());

        onView(withId(R.id.tv_detailed_name)).check(matches(isDisplayed())).check(matches(isDisplayed())).check(matches(withText(TEST_NAME_EDITED)));
        onView(withId(R.id.tv_detailed_description)).check(matches(isDisplayed())).check(matches(isDisplayed())).check(matches(withText(TEST_DESC_EDITED)));
        onView(withId(R.id.tv_detailed_category)).check(matches(isDisplayed())).check(matches(isDisplayed())).check(matches(withText(TEST_CATEGORY_EDITED)));
    }

    @Test
    public void buttonViewInMapTest() {
        onView(withId(R.id.bt_view_on_map)).perform(click());

    }

    @Test
    public void buttonDeleteTest() {

        onView(withId(R.id.bt_delete_track)).perform(click());

        //   onView(withId(R.id.bt_track_list)).check(matches(isDisplayed()));
    }

    @After
    public void cleaning() {

        if (TrackDao.getInstance().getTrack(trackId) != null) {
            TrackDao.getInstance().deleteTrack(trackId);
        }
    }

    long createTrack() {
        Track track = new Track(TEST_NAME, TEST_DESC, TEST_CATEGORY);
        track.setStartTime(TEST_START_TIME);
        track.setStopTime(TEST_STOP_TIME);

        return TrackDao.getInstance().createTrack(track);
    }
}
