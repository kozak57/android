package com.example.kozak.gpstracker.uitest;


import android.content.Context;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.kozak.gpstracker.GPSTrackerApplication;
import com.example.kozak.gpstracker.R;
import com.example.kozak.gpstracker.activity.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasErrorText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;


@RunWith(AndroidJUnit4.class)
public class UITest {


    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void test() {
        Context context = GPSTrackerApplication.getAppContext();
        String string = context.getResources().getString(R.string.error_enter_name);
        onView(withId(R.id.fab_prepare_track)).perform(click());


        onView(withId(R.id.et_preparing_track_name)).check(matches(isDisplayed()));

        onView(withId(R.id.fab_create_track)).perform(click());
        onView(withId(R.id.et_preparing_track_name)).check(matches(hasErrorText(string)));
    }
}
