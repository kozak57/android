package com.example.kozak.gpstracker.activity;

import android.content.Intent;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.kozak.gpstracker.R;
import com.example.kozak.gpstracker.dao.TrackDao;
import com.example.kozak.gpstracker.model.Track;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class TrackListActivityTest {

    private static final String TEST_NAME = "listTest name";
    private static final String TEST_DESC = "listTestTest description";
    private static final String TEST_CATEGORY = "listTestTest category";
    private static final Date TEST_START_TIME = new Date();
    private static final Date TEST_STOP_TIME = new Date(TEST_START_TIME.getTime() + 5000);
    private static long trackId;


    public void preparing() {
        Track track = new Track(TEST_NAME, TEST_DESC, TEST_CATEGORY);
        track.setStartTime(TEST_START_TIME);
        track.setStopTime(TEST_STOP_TIME);

        trackId = TrackDao.getInstance().createTrack(track);

    }

    @Rule
    public ActivityTestRule<TrackListActivity> activityTestRule = new ActivityTestRule<TrackListActivity>(TrackListActivity.class) {
        @Override
        protected Intent getActivityIntent() {
            preparing();
            return super.getActivityIntent();


        }
    };

    @Test
    public void checkActivity() {
        onView(withId(R.id.recycler_track_view)).check(matches(isDisplayed())).check(matches(isDisplayed()));

    }

    @Test
    public void scrollTest() {

        //scrollTo works only with unique texts
        onView(withId(R.id.recycler_track_view)).perform(RecyclerViewActions.scrollTo(hasDescendant(withText(TEST_NAME))));
        onView(withId(R.id.recycler_track_view)).check(matches(hasDescendant(withText(TEST_NAME))));
    }

    @Test
    public void itemClickTest() {
        onView(withId(R.id.recycler_track_view)).perform(RecyclerViewActions.scrollTo(hasDescendant(withText(TEST_NAME))));
        onView(withId(R.id.recycler_track_view)).perform(RecyclerViewActions.actionOnItem(hasDescendant(withText(TEST_NAME)), click()));

        onView(withId(R.id.tv_detailed_name)).check(matches(withText(TEST_NAME)));
    }

    @After
    public void cleaning() {
        TrackDao.getInstance().deleteTrack(trackId);
    }
}





