package com.example.kozak.gpstracker.util;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public class StringFormatterTest {

    @Test
    public void getRoundedDoubleTest() {
        assertThat(StringFormatter.getRoundedDouble(1515.1515, 2), is("1515.15"));
        assertThat(StringFormatter.getRoundedDouble(1515.1515, 0), is("1515"));
        assertThat(StringFormatter.getRoundedDouble(1515.1115, 1), is("1515.1"));
        assertThat(StringFormatter.getRoundedDouble(1515.1515, -5), is("0"));
    }

    @Test
    public void getTimePeriodTest() {
        assertThat(StringFormatter.getTimePeriod(1000L), is("1 second"));
        assertThat(StringFormatter.getTimePeriod(2 * 1000L), is("2 seconds"));
        assertThat(StringFormatter.getTimePeriod(60 * 1000L), is("1 minute"));
        assertThat(StringFormatter.getTimePeriod(125 * 1000L), is("2 minutes, 5 seconds"));
        assertThat(StringFormatter.getTimePeriod(60 * 60 * 1000L), is("1 hour"));
        assertThat(StringFormatter.getTimePeriod(2 * 60 * 60 * 1000L), is("2 hours"));
        assertThat(StringFormatter.getTimePeriod(2 * 60 * 60 * 1000L), is("2 hours"));
        assertThat(StringFormatter.getTimePeriod(24 * 60 * 60 * 1000L + 1000L), is("24 hours, 1 second"));

    }
}
