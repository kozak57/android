package com.example.kozak.gpstracker.activity;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.kozak.gpstracker.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isClickable;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void checkActivity() {
        onView(withId(R.id.fab_prepare_track)).check(matches(isDisplayed())).check(matches(isClickable()));

        onView(withId(R.id.fragment)).check(matches(isDisplayed()));
        onView(withId(R.id.bt_track_list)).check(matches(isDisplayed()));
    }

    @Test
    public void prepareTrackActivity() {
        onView(withId(R.id.fab_prepare_track)).perform(click());

        onView(withId(R.id.et_preparing_track_name)).check(matches(isDisplayed())).check(matches(isClickable()));
    }

    @Test
    public void trackListButton() {
        onView(withId(R.id.bt_track_list)).perform(click());
        onView(withId(R.id.recycler_track_view)).check(matches(isDisplayed()));
    }
}
