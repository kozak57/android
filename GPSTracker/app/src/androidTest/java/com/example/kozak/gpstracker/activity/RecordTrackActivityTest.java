package com.example.kozak.gpstracker.activity;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.kozak.gpstracker.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isClickable;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class RecordTrackActivityTest {

    public static final String TEST_NAME = "Test name";
    public static final String TEST_DESC = "Test description";
    public static final String TEST_CATEGORY = "Test category";

    @Rule
    public ActivityTestRule<RecordTrackActivity> activityRule =
            new ActivityTestRule<RecordTrackActivity>(RecordTrackActivity.class) {
                @Override
                protected Intent getActivityIntent() {
                    Context targetContext = InstrumentationRegistry.getInstrumentation()
                            .getTargetContext();
                    Intent result = new Intent(targetContext, MainActivity.class);
                    result.putExtra("name", TEST_NAME);
                    result.putExtra("desc", TEST_DESC);
                    result.putExtra("category", TEST_CATEGORY);
                    return result;
                }
            };

    @Test
    public void checkActivity() {
        onView(withId(R.id.bt_record_track_start_pause)).check(matches(isDisplayed())).check(matches(isClickable()));
        onView(withId(R.id.bt_record_track_stop)).check(matches(isDisplayed())).check(matches(isClickable()));
    }

    @Test
    public void buttonStartPauseTest() {
        onView(withId(R.id.bt_record_track_start_pause)).check(matches(withText(R.string.gps_recording_pause)));
        onView(withId(R.id.bt_record_track_start_pause)).perform(click());

        onView(withId(R.id.bt_record_track_start_pause)).check(matches(withText(R.string.gps_recording_continue)));
        onView(withId(R.id.bt_record_track_start_pause)).perform(click());

        onView(withId(R.id.bt_record_track_start_pause)).check(matches(withText(R.string.gps_recording_pause)));
    }

    @Test
    public void buttonFinishTest() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.bt_record_track_stop)).perform(click());

        onView(withId(R.id.tv_detailed_distance)).check(matches(isDisplayed()));

        onView(withId(R.id.tv_detailed_name)).check(matches(withText(TEST_NAME)));
        onView(withId(R.id.tv_detailed_description)).check(matches(withText(TEST_DESC)));
        onView(withId(R.id.tv_detailed_category)).check(matches(withText(TEST_CATEGORY)));

        onView(withId(R.id.bt_delete_track)).perform(click());
    }
}
