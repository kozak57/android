package com.example.kozak.gpstracker.dao;

import android.database.Cursor;
import android.support.test.runner.AndroidJUnit4;

import com.example.kozak.gpstracker.model.ContractClass;
import com.example.kozak.gpstracker.model.GPSData;
import com.example.kozak.gpstracker.model.Track;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public class TrackDaoTest {
    private TrackDao trackDao;
    private long trackId;
    private static final String TEST_NAME = "Test name";
    private static final String TEST_DESC = "Test desc";
    private static final String TEST_CATEGORY = "Test cat";

    @Before
    public void preparing() {
        trackDao = new TrackDao(new TracksSQLiteHelper(null));
        trackId = trackDao.createTrack(new Track(TEST_NAME, TEST_DESC, TEST_CATEGORY));
    }

    @Test
    public void getTrackTest() {
        assertThat(trackDao.getTrack(trackId).getName(), is(TEST_NAME));
        assertThat(trackDao.getTrack(trackId).getDescription(), is(TEST_DESC));
        assertThat(trackDao.getTrack(trackId).getCategory(), is(TEST_CATEGORY));
    }

    @Test
    public void createTrackTest() {
        trackId = trackDao.createTrack(new Track(TEST_NAME, TEST_DESC, TEST_CATEGORY));

        assertThat(trackDao.getTrack(trackId).getName(), is(TEST_NAME));
        assertThat(trackDao.getTrack(trackId).getDescription(), is(TEST_DESC));
        assertThat(trackDao.getTrack(trackId).getCategory(), is(TEST_CATEGORY));
    }

    @Test
    public void editTrackTest() {
        Track track = trackDao.getTrack(trackId);
        track.setName("edited " + TEST_NAME);
        track.setDescription("edited " + TEST_DESC);
        track.setCategory("edited " + TEST_CATEGORY);

        trackDao.editTrack(trackId, track);

        assertThat(trackDao.getTrack(trackId).getName(), is("edited " + TEST_NAME));
        assertThat(trackDao.getTrack(trackId).getDescription(), is("edited " + TEST_DESC));
        assertThat(trackDao.getTrack(trackId).getCategory(), is("edited " + TEST_CATEGORY));
    }

    @Test
    public void deleteTest() {
        trackDao.deleteTrack(trackId);
        assertThat(trackDao.getTrack(trackId), nullValue());
    }

    @Test
    public void getTrackCountTest() {
        assertThat(trackDao.getTrackCount(), is(3));
    }

    @Test
    public void getAllTracksInCursorTest() {
        Cursor cursor = trackDao.getAllTracksInCursor();
        cursor.moveToFirst();
        assertThat(cursor.getCount(), notNullValue());
        assertThat(cursor.getString(cursor.getColumnIndex(ContractClass.Tracks.COLUMN_NAME_NAME)), is(trackDao.getTrack(1).getName()));
        assertThat(cursor.getString(cursor.getColumnIndex(ContractClass.Tracks.COLUMN_NAME_DESC)), is(trackDao.getTrack(1).getDescription()));
        assertThat(cursor.getString(cursor.getColumnIndex(ContractClass.Tracks.COLUMN_NAME_CATEGORY)), is(trackDao.getTrack(1).getCategory()));
    }

    @Test
    public void getAllTracksTest() {
        List<Track> tracks = trackDao.getAllTracks();

        assertThat(tracks.size(), is(3));

        Track track = tracks.get(2);

        assertThat(track.getName(), is(TEST_NAME));
        assertThat(track.getDescription(), is(TEST_DESC));
        assertThat(track.getCategory(), is(TEST_CATEGORY));
    }

    @Test
    public void getTrackDistanceTest() {
        assertThat(trackDao.getTrackDistance(1), is(4051.987060546875));
        assertThat(trackDao.getTrackDistance(2), is(0.0));
    }

    @Test
    public void getTotalDistanceTest() {
        double distance1 = trackDao.getTrackDistance(1);
        double distance2 = trackDao.getTrackDistance(2);
        assertThat(trackDao.getTotalDistance(), is(distance1 + distance2));
    }

    @Test
    public void getTrackTimeTest() {
        Track track = new Track(TEST_NAME, TEST_DESC, TEST_CATEGORY);
        track.setStartTime(new Date());
        track.setStopTime(new Date(track.getStartTime().getTime() + 5000L));

        trackId = trackDao.createTrack(track);

        assertThat(trackDao.getTrackTime(trackId), is(5000L));
    }

    @Test
    public void getAverageSpeedTest() {
        List<GPSData> gpsDataList = trackDao.getGPSDataByTrackId(1);
        double avgSpeed = 0.0;

        for (int i = 0; i < gpsDataList.size(); i++) {
            avgSpeed += gpsDataList.get(i).getSpeed();
        }
        avgSpeed = avgSpeed / gpsDataList.size();

        assertThat(trackDao.getTrackAverageSpeed(1), is(avgSpeed));
    }

    @Test
    public void createGpsDataTest() {
        final double value = 45.15615;
        final Date date = new Date();
        GPSData gpsData = new GPSData();

        gpsData.setLongitude(value);
        gpsData.setLatitude(value);
        gpsData.setAltitude(value);
        gpsData.setSpeed(value);
        gpsData.setAccuracy(value);
        gpsData.setBearing(value);
        gpsData.setDate(date);
        gpsData.setTrackId(trackId);

        trackDao.createGpsData(gpsData);

        GPSData receivedGpsData = trackDao.getGPSDataByTrackId(trackId).get(0);

        assertThat(receivedGpsData.getLongitude(), is(value));
        assertThat(receivedGpsData.getLatitude(), is(value));
        assertThat(receivedGpsData.getAltitude(), is(value));
        assertThat(receivedGpsData.getAccuracy(), is(value));
        assertThat(receivedGpsData.getBearing(), is(value));
        assertThat(receivedGpsData.getSpeed(), is(value));
        assertThat(receivedGpsData.getDate(), is(date));
    }
}
