package com.example.kozak.gpstracker.activity;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.kozak.gpstracker.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasErrorText;
import static android.support.test.espresso.matcher.ViewMatchers.isClickable;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class PrepareTrackActivityTest {

    @Rule
    public ActivityTestRule<PrepareTrackActivity> activityTestRule = new ActivityTestRule<>(PrepareTrackActivity.class);

    @Test
    public void checkActivity() {
        onView(withId(R.id.fab_create_track)).check(matches(isDisplayed())).check(matches(isClickable()));

        onView(withId(R.id.et_preparing_track_name)).check(matches(isDisplayed())).check(matches(isClickable()));
        onView(withId(R.id.et_preparing_track_desc)).check(matches(isDisplayed())).check(matches(isClickable()));
        onView(withId(R.id.et_preparing_track_category)).check(matches(isDisplayed())).check(matches(isClickable()));
    }

    @Test
    public void errorCheck() {
        String nameError = activityTestRule.getActivity().getString(R.string.prepare_track_name_error);
        String descError = activityTestRule.getActivity().getString(R.string.prepare_track_desc_error);
        String categoryError = activityTestRule.getActivity().getString(R.string.prepare_track_category_error);

        onView(withId(R.id.fab_create_track)).perform(click());
        onView(withId(R.id.et_preparing_track_name)).check(matches(hasErrorText(nameError)));
        onView(withId(R.id.et_preparing_track_name)).perform(typeText("Test name")).perform(closeSoftKeyboard());

        onView(withId(R.id.fab_create_track)).perform(click());
        onView(withId(R.id.et_preparing_track_desc)).check(matches(hasErrorText(descError)));
        onView(withId(R.id.et_preparing_track_desc)).perform(typeText("Test description")).perform(closeSoftKeyboard());

        onView(withId(R.id.fab_create_track)).perform(click());
        onView(withId(R.id.et_preparing_track_category)).check(matches(hasErrorText(categoryError)));
        onView(withId(R.id.et_preparing_track_category)).perform(typeText("Test category")).perform(closeSoftKeyboard());

        onView(withId(R.id.fab_create_track)).perform(click());
        onView(withId(R.id.bt_record_track_start_pause)).check(matches(isDisplayed()));
    }
}
