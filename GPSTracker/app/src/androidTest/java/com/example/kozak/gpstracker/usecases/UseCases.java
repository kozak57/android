package com.example.kozak.gpstracker.usecases;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.kozak.gpstracker.R;
import com.example.kozak.gpstracker.activity.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class UseCases {

    private static final String TEST_NAME = "Test name Use case";
    private static final String TEST_DESC = "Test description";
    private static final String TEST_CATEGORY = "Test category";

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);


    @Test
    public void createViewDeleteTrack() {
        onView(withId(R.id.fab_prepare_track)).perform(click());

        onView(withId(R.id.et_preparing_track_name)).perform(typeText(TEST_NAME));
        onView(withId(R.id.et_preparing_track_desc)).perform(typeText(TEST_DESC));
        onView(withId(R.id.et_preparing_track_category)).perform(typeText(TEST_CATEGORY)).perform(closeSoftKeyboard());
        onView(withId(R.id.fab_create_track)).perform(click());

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.bt_record_track_stop)).perform(click());

        onView(withId(R.id.bt_delete_track)).perform(click());
    }


}
