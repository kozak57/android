package com.example.animation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.hasFixedSize();

        List<String> strings = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            strings.add("Item #" + String.valueOf(i));
        }

        MyAdapter myAdapter = new MyAdapter(strings);

        recyclerView.setAdapter(myAdapter);

        RecyclerView.LayoutManager linearLayoutManager = new MyLayoutManager(getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);

    }
}
