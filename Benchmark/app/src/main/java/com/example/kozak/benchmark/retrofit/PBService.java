package com.example.kozak.benchmark.retrofit;


import com.example.kozak.benchmark.CurrencyInfo;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;

public interface PBService {

    @GET("p24api/pubinfo?json&exchange&coursid=5")
    Call<List<CurrencyInfo>> getContent();
}
