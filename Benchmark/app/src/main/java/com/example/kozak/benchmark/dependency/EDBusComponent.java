package com.example.kozak.benchmark.dependency;

import android.app.Application;

import com.example.kozak.benchmark.MainActivity;
import com.example.kozak.benchmark.edbus.EDBus;
import com.example.kozak.benchmark.retrofit.RetrofitCurrencyService;
import com.example.kozak.benchmark.volley.VolleyService;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = EDBusModule.class)
@Singleton
public interface EDBusComponent {
    EDBus getEDBus();

    void inject(Application benchmarkApplication);

    void inject(MainActivity activity);

    void inject(VolleyService service);

    void inject(RetrofitCurrencyService service);
}
