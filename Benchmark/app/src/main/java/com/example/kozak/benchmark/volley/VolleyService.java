package com.example.kozak.benchmark.volley;

import android.app.IntentService;
import android.content.Intent;

import com.example.kozak.benchmark.dependency.DaggerEDBusComponent;
import com.example.kozak.benchmark.edbus.EDBus;
import com.example.kozak.benchmark.edbus.ServiceCompleteTimeEvent;

import java.util.Date;

import javax.inject.Inject;

public class VolleyService extends IntentService {
    @Inject
    EDBus edBus;

    public VolleyService() {
        super("");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        DaggerEDBusComponent.create().inject(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        MyVolley volley = MyVolley.getInstance(getBaseContext());
        Date start = new Date();
        for (int i = 0; i < 100; i++) {
            volley.volleyRequest();
        }
        Date stop = new Date();
        edBus.getInstance().post(new ServiceCompleteTimeEvent(stop.getTime() - start.getTime()));

    }
}


