package com.example.kozak.benchmark.volley;

import android.content.Context;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.kozak.benchmark.CurrencyInfo;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;


public class MyVolley {

    public static final String TAG = "MyVolley";
    private static MyVolley INSTANCE;
    private Context context;
    List<CurrencyInfo> currencyInfoList;

    private MyVolley(Context context) {
        this.context = context;
    }

    public static synchronized MyVolley getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new MyVolley(context);
        }
        return INSTANCE;
    }

    public void volleyRequest() {

        RequestQueue queue = Volley.newRequestQueue(context);
        String url = "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5";
        Log.d(TAG, "prepare Url: " + url);

        currencyInfoList = new ArrayList<>();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {

                for (int i = 0; i < response.length(); i++) {
                    CurrencyInfo currencyInfo = new CurrencyInfo();
                    try {
                        currencyInfo.setCcy(response.getJSONObject(i).getString("ccy"));
                        currencyInfo.setBuy(response.getJSONObject(i).getString("buy"));
                        currencyInfo.setSale(response.getJSONObject(i).getString("sale"));
                        currencyInfoList.add(currencyInfo);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                // EDBus.getBusInstance().post(new DataLoadedEvent(currencyInfoList));
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "response error: " + error.toString());
            }
        });

        queue.add(jsonArrayRequest);
    }
}

