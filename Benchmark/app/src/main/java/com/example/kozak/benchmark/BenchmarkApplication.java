package com.example.kozak.benchmark;

import android.app.Application;

import com.example.kozak.benchmark.dependency.DaggerEDBusComponent;

public class BenchmarkApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        DaggerEDBusComponent.create().inject(this);
    }

}
