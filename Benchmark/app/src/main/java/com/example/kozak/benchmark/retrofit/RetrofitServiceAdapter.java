package com.example.kozak.benchmark.retrofit;


import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class RetrofitServiceAdapter {
    private static final String BASE_URL = "https://api.privatbank.ua";

    private PBService service;

    public RetrofitServiceAdapter() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(PBService.class);

    }

    public PBService getService() {
        return service;
    }


}
