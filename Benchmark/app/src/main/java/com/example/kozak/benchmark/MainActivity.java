package com.example.kozak.benchmark;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;

import com.example.kozak.benchmark.dependency.DaggerEDBusComponent;
import com.example.kozak.benchmark.edbus.DataLoadedEvent;
import com.example.kozak.benchmark.edbus.EDBus;
import com.example.kozak.benchmark.edbus.ServiceCompleteTimeEvent;
import com.example.kozak.benchmark.retrofit.RetrofitCurrencyService;
import com.example.kozak.benchmark.volley.VolleyService;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "MainActivity";
    private CurrencyAdapter adapter;
    private boolean isVolleyChecked;
    private boolean isRetrofitChecked;
    @Inject
    EDBus edBus;

    @Bind(R.id.tv_retrofit_speed)
    TextView rertofitUpdateTime;
    @Bind(R.id.tv_volley_speed)
    TextView volleyUpdateTime;
    @Bind(R.id.currency_list)
    ListView currencyList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DaggerEDBusComponent.create().inject(this);

        ButterKnife.bind(this);

        adapter = new CurrencyAdapter(this);
        currencyList.setAdapter(adapter);
    }

    @OnClick(R.id.bt_volley_speed_update)
    void onClickVolley() {
        Intent volleyService = new Intent(getBaseContext(), VolleyService.class);
        isRetrofitChecked = false;
        isVolleyChecked = true;

        startService(volleyService);
    }

    @OnClick(R.id.bt_retrofit_speed_update)
    void onClickRetrofit() {
        Intent retrofitService = new Intent(getBaseContext(), RetrofitCurrencyService.class);
        isRetrofitChecked = true;
        isVolleyChecked = false;

        startService(retrofitService);
    }

    @Override
    protected void onPause() {
        super.onPause();
        edBus.getInstance().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        edBus.getInstance().register(this);
    }

    @Subscribe
    public void updateData(DataLoadedEvent dataLoadedEvent) {
        adapter.setDataListCurrencyInfo(dataLoadedEvent.getCurrencyInfo());
    }

    @Subscribe
    public void updateText(ServiceCompleteTimeEvent serviceCompleteTimeEvent) {
        if (isRetrofitChecked) {
            rertofitUpdateTime.setText(String.valueOf(serviceCompleteTimeEvent.getCompleteTime()));
        }
        if (isVolleyChecked) {
            volleyUpdateTime.setText(String.valueOf(serviceCompleteTimeEvent.getCompleteTime()));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(CurrencyAdapter.class);
    }
}
