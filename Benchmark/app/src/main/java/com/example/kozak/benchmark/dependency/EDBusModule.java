package com.example.kozak.benchmark.dependency;

import com.example.kozak.benchmark.edbus.EDBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class EDBusModule {

    @Singleton
    @Provides
    public EDBus provideEDBusInstance() {
        return new EDBus();
    }
}
