package com.example.kozak.benchmark.retrofit;

import android.app.IntentService;
import android.content.Intent;

import com.example.kozak.benchmark.CurrencyInfo;
import com.example.kozak.benchmark.dependency.DaggerEDBusComponent;
import com.example.kozak.benchmark.edbus.DataLoadedEvent;
import com.example.kozak.benchmark.edbus.EDBus;
import com.example.kozak.benchmark.edbus.ServiceCompleteTimeEvent;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit.Response;

public class RetrofitCurrencyService extends IntentService {

    @Inject
    EDBus edBus;

    @Override
    public void onCreate() {
        super.onCreate();
        DaggerEDBusComponent.create().inject(this);
    }

    public RetrofitCurrencyService() {
        super("");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        RetrofitServiceAdapter retrofitServiceAdapter = new RetrofitServiceAdapter();
        Response<List<CurrencyInfo>> listResponse = null;

        Date start = null;
        Date stop = null;
        try {
            start = new Date();
            for (int i = 0; i < 10; i++) {
                listResponse = retrofitServiceAdapter.getService().getContent().execute();
            }
            stop = new Date();
        } catch (IOException e) {
            e.printStackTrace();
        }

        edBus.getInstance().post(new ServiceCompleteTimeEvent(stop.getTime() - start.getTime()));
        assert listResponse != null;
        edBus.getInstance().post(new DataLoadedEvent(listResponse.body()));
    }
}