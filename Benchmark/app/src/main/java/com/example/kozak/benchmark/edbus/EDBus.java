package com.example.kozak.benchmark.edbus;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.squareup.otto.Bus;

public class EDBus extends Bus {
    public static final String TAG = "EDBus";
    private static volatile EDBus bus;
    private final Handler handler = new Handler(Looper.getMainLooper());

    public EDBus getInstance() {
        if (bus == null) {
            synchronized (EDBus.class) {
                if (bus == null) {
                    bus = new EDBus();
                }
            }
        }
        Log.d(TAG, "EDBus instance returned");
        return bus;
    }

    @Override
    public void post(final Object event) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            super.post(event);
        } else {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    EDBus.super.post(event);
                }
            });
        }
        Log.d(TAG, "data posted");
    }
}