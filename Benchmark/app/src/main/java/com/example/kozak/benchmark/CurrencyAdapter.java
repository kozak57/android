package com.example.kozak.benchmark;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class CurrencyAdapter extends BaseAdapter {
    public static final String TAG = "CurrencyAdapter";

    public CurrencyAdapter(Context context) {
        this.context = context;
    }

    Context context;
    List<CurrencyInfo> currencyInfoList = new ArrayList<>();

    @Override
    public int getCount() {
        return currencyInfoList.size();
    }

    @Override
    public Object getItem(int position) {
        return currencyInfoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.currency_layout, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        TextView ccyTextView = viewHolder.getCcyTextView();
        TextView buyTextView = viewHolder.getBuyTextView();
        TextView saleTextView = viewHolder.getSaleTextView();

        view.setTag(viewHolder);

        ccyTextView.setText(currencyInfoList.get(position).getCcy());
        buyTextView.setText(currencyInfoList.get(position).getBuy());
        saleTextView.setText(currencyInfoList.get(position).getSale());
        Log.d(TAG, "Creating view in adapter");
        return view;
    }

    static class ViewHolder {
        @Bind(R.id.ccy)
        TextView ccyTextView;
        @Bind(R.id.buy)
        TextView buyTextView;
        @Bind(R.id.sale)
        TextView saleTextView;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        public TextView getCcyTextView() {
            return ccyTextView;
        }

        public TextView getBuyTextView() {
            return buyTextView;
        }

        public TextView getSaleTextView() {
            return saleTextView;
        }
    }

    public void setDataListCurrencyInfo(List<CurrencyInfo> listCurrencyInfo) {
        Log.d(TAG, "Set Data List Currency Info");
        this.currencyInfoList = listCurrencyInfo;
        notifyDataSetChanged();
    }

}
