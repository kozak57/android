package com.example.kozak.benchmark.edbus;

import android.util.Log;

import com.example.kozak.benchmark.CurrencyInfo;

import java.util.List;

public class DataLoadedEvent {
    private static final String TAG = "DataLoadedEvent";
    private List<CurrencyInfo> currencyInfo;

    public DataLoadedEvent(List<CurrencyInfo> currencyInfo) {

        Log.d(TAG, "data loaded");
        this.currencyInfo = currencyInfo;
    }

    public List<CurrencyInfo> getCurrencyInfo() {
        Log.d(TAG, "data returned");
        return currencyInfo;
    }
}