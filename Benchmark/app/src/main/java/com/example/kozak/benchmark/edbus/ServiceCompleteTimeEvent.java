package com.example.kozak.benchmark.edbus;


public class ServiceCompleteTimeEvent {
    public ServiceCompleteTimeEvent(long ms) {
        this.ms = ms;
    }

    private long ms;

    public long getCompleteTime() {
        return ms;
    }
}
